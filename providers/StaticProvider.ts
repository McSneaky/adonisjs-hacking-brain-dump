import { ApplicationContract } from '@ioc:Adonis/Core/Application'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import staticServer from 'serve-static'
import fs from 'fs'
import Stream from 'stream'


export default class StaticProvider {
  public static needsApplication = true

  constructor (protected app: ApplicationContract) {
  }

  public register () {
    // Register your own bindings
  }

  public async boot () {
    // IoC container is ready
    console.log('here')
    this.app.container.with(
      ['Adonis/Core/Server'],
      (server) => {
        console.log('I\'m here beans!')
        const coolClass = new CoolClass()
        // server.hooks.before(coolClass.handle.bind(coolClass))
      }
    )
  }

  public async ready () {
    // App is ready
  }

  public async shutdown () {
    // Cleanup, since app is going down
  }
}

class CoolClass {
  private serve = staticServer('./public/', { fallthrough: false})
  constructor () {
  }

  public async handle({ request, response }: HttpContextContract) {
    let stream = fs.createWriteStream('/home/mcsneaky/Projects/Ap3k/Node_modules/tmp/path')

    // response.response
    // let tempResponse =
    let tempStream = new Stream.PassThrough({
      write(chunk) {
        console.log('Writing');
        this.write(chunk)
      },
      read(size) {
        console.log('reading');
        return this.read(size)
      }
    })

    // tempStream.getHeader = response.getHeader
    // tempStream.setHeader = response.response.setHeader
    // tempStream.writeHead = response.response.writeHead
    // tempStream.getHeader = () => {}
    // tempStream.setHeader = () => {}


    // let tempStream = new Stream.Writable({
    //   write(chunk) {
    //     this.write(chunk)
    //   }
    // })

    this.serve(request.request, tempResponse, (err) => {
      console.trace(err)
    })
    tempResponse.end('hurr')
    // response.response.destroy()
    tempStream.pipe(response.response)

    // response.response = tempStream

    // request.response.end('Hurr')

    // response.response = tempResponse
    // response.response.write(tempStream)
    // tempStream.write('hurr')
    // tempStream.pipe(stream)
    // tempStream.write('durr')

    await new Promise((res, rej) => setTimeout(() => { res(true)}, 300))
    // response.response.pipe(tempStream)


    // stream.write('hurr')
    // console.log(stream);


    // response.response.on('pipe', (data) => {
    //   // console.log(data);
    //   // data.pipe(stream)
    //   // data.pipe(response.response)
    //   // response.response.write('AAAAAAAAAAAAAA')
    // })

    // console.log(response.response.pipe(stream));


    // response.response.addListener('finish', (ctx) => {
    //   console.log(ctx);
    //   console.log('Do stuff in here now');
    // })
  }

//   public async handle ({ request, response }: HttpContextContract): Promise<void> {
//     return new Promise((resolve) => {
//       function next () {
//         response.response.removeListener('finish', next)
//         console.log('What?')
//         resolve()
//       }

//       /**
//        * Whether or not the file has been served by serve static, we
//        * will cleanup the finish event listener.
//        *
//        * 1. If file has been served, then the `finish` callback get invoked.
//        * 2. If file has not been served, then callback (3rd argument) will
//        *    get invoked.
//        */
//       response.response.addListener('finish', next)
//       this.serve(request.request, response.response, next)
//     })
//   }
// }
